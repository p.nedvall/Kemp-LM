PRTG Device Template for Kemp-LM devices
===========================================

This project contains all the files necessary to integrate the Kemp LoadMaster
into PRTG for auto discovery and sensor creation.


Download Instructions

[A zip file containing all the files in the project can be downloaded from the 
repository](https://gitlab.com/PRTG/Device-Templates/Kemp-LM/-/jobs/artifacts/master/download?job=PRTGDistZip)

Installation Instructions


The template project has a standard directory structure:
All the files in the PRTG subdirectory needs to go into the PRTG program directory
(https://kb.paessler.com/en/topic/463-how-and-where-does-prtg-store-its-data).
The other files are for documentation and testing.


Adddding a New template

Create a new template by creating project under:
https://gitlab.com/PRTG-Projects/Device-Templates
give it a name related to the device manufacturer by Import Project from "Repo URL"
using: https://uid:pwd@gitlab.com/PRTG-Projects/BaseTemplateProject.git
